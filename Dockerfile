## Stage 1: Compile and Build angular codebase
# see:  https://www.indellient.com/blog/how-to-dockerize-an-angular-application-with-nginx/
#       https://dzone.com/articles/how-to-dockerize-angular-app

# Use official node image as the base image
FROM node:14 as build

# Set the working directory
WORKDIR /usr/local/app

# Add the source code to app
RUN rm -f package.json.lock
COPY ./ /usr/local/app/

# Install all the dependencies
RUN npm install -g yarn --force
# Generate the build of the application
RUN yarn run build -c production


## Stage 2: Serve app with nginx server

# Use official nginx image as the base image
FROM nginx:1.21.3-alpine
## Copy our default nginx config
COPY nginx/default.conf /etc/nginx/conf.d/
## Remove default nginx website
RUN rm -rf /usr/share/nginx/html/*
# Copy the build output to replace the default nginx contents.
COPY --from=build /usr/local/app/dist/cds-client /usr/share/nginx/html
CMD ["nginx", "-g", "daemon off;"]

# Expose port 80
EXPOSE 80