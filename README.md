## CDS Client ##
# <your registry or username>/<image name>:<tag>
> docker build -t blabcr.azurecr.io/cds-client:latest .
or
> docker build -t blabcr.azurecr.io/cds-client:latest . --no-cache

# Start
# set context back to default
> docker context use default 
> docker run -d -p 8080:80 blabcr.azurecr.io/cds-client:latest .

# Stop
> docker ps
> docker stop {{container ID}}

# Push
# see: https://docs.microsoft.com/en-us/azure/container-registry/container-registry-get-started-docker-cli?tabs=azure-cli
Use VSCode Docker GUI to push image
alternative: 
> docker tag cds-client blabcr.azurecr.io/cds-client
> docker push blabcr.azurecr.io/cds-client

## Configure ##
# update cli permissions
# see: https://docs.microsoft.com/en-us/azure/azure-resource-manager/troubleshooting/error-register-resource-provider
> az login
> az provider list
> az provider register --namespace Microsoft.DataFactory
> az provider register --namespace Microsoft.Web
# setup traffic manager
# see: https://docs.microsoft.com/en-us/azure/app-service/configure-domain-traffic-manager
* Navigate to "traffic manager" and create container endpoint
* Navigate to "container" app, enable paid tier and create "custom domain"
# map custom DNS name
* Navigate to "domain.com" and add CNAME "@" for container URL






<!-- ---- -->


<!-- ## Build on Azure directly (?) ##
## Setup Azure CR ##
# see:  https://code.visualstudio.com/docs/containers/app-service
#       https://docs.docker.com/cloud/aci-integration/
> docker login azure
> docker context create aci blab-aci-context
> docker use aci blab-aci-context
> docker ps
# set context back to default
> docker context use default -->

