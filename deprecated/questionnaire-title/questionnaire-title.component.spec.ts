import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireTitleComponent } from './questionnaire-title.component';

describe('QuestionnaireTitleComponent', () => {
  let component: QuestionnaireTitleComponent;
  let fixture: ComponentFixture<QuestionnaireTitleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionnaireTitleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireTitleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
