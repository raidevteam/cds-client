import { Injectable } from '@angular/core';
import { Questionnaire } from '../../models/questionnaire.model';



@Injectable({
  providedIn: 'root'
})
export class StorageService {

  private localStorage: Storage;

  constructor() {
    // set defaults
    this.localStorage = window.localStorage;
  }

  get isLocalStorageSupported(): boolean {
    return !!this.localStorage;
  }

  get(mrn: number): Questionnaire {
    if (!this._throwsIfLocalStorageUndefined()) { return; }
    const value = this.localStorage.getItem(String(mrn));
    return (value) ? JSON.parse(value) : undefined;
  }

  getAll(): Questionnaire[] {
    const keys = Object.keys(localStorage);
    const values = keys.map(key => this.get(parseInt(key)));
    return values;
  }

  update(mrn: number, value: Questionnaire): boolean {
    if (!this._throwsIfLocalStorageUndefined() || !value) { return false; }
    const curr = this.get(mrn) || [];
    const updated = Object.assign(curr, value) as Questionnaire;
    this.set(mrn, updated);
    return true;
  }

  set(mrn: number, value: Questionnaire): boolean {
    if (!this._throwsIfLocalStorageUndefined()) { return false; }
    this.localStorage.setItem(String(mrn), JSON.stringify(value));
    return true;
  }

  remove(mrn: string | number): boolean {
    if (!this._throwsIfLocalStorageUndefined()) { return false; }
    this.localStorage.removeItem(`${mrn}`);
    return true;
  }

  storageSize() {
    if (!this._throwsIfLocalStorageUndefined()) { return; }
    let allStrings = '';
    for (var key in window.localStorage) {
      if (window.localStorage.hasOwnProperty(key)) {
        allStrings += window.localStorage[key];
      }
    }
    return allStrings ? 3 + ((allStrings.length * 16) / (8 * 1024)) + ' KB' : 'Empty (0 KB)';
  }

  /**
   * HELPERS
   */
  private _throwsIfLocalStorageUndefined(): boolean {
    if (!this.isLocalStorageSupported) {
      throw `StorageService: warning, local storage not supported`;
    }
    return true;
  }

}

//----------------------------------

// private _sampleData() {
//   return [
//     new Annotation({
//       docID: uuidv4(),
//       pageIdx: 0,
//       type: AnnotationType.LINE,
//       // value: [800, 200, 388, 399]
//       rect: [34, 34, 218, 302],
//       lineCoordinates: [34, 34, 218, 302],
//     }),
//   ];
// }
