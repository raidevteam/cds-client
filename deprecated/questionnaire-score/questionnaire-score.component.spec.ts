import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionnaireScoreComponent } from './questionnaire-score.component';

describe('QuestionnaireScoreComponent', () => {
  let component: QuestionnaireScoreComponent;
  let fixture: ComponentFixture<QuestionnaireScoreComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionnaireScoreComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionnaireScoreComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
