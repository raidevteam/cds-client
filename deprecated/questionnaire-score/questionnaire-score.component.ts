import { Component, OnInit, Input } from '@angular/core';


@Component({
  selector: 'app-questionnaire-score',
  templateUrl: './questionnaire-score.component.html',
  styleUrls: ['./questionnaire-score.component.scss']
})
export class QuestionnaireScoreComponent implements OnInit {

  @Input() score: number;

  constructor() { }

  public get risk(): string {
    if (this.score === -1) { return "NA"; }
    return (this.score >= 8) ? "High" : "Low";
  }

  public get action(): string {
    if (this.score === -1) { return; }
    return (this.score >= 8) ? "Yes" : "No";
  }

  ngOnInit(): void { }
}
