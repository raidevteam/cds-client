import * as moment from 'moment';
import { Questions } from '../core/models/fhir-questionnaire/questions.model';
import { Item } from '../core/models/fhir-questionnaire/item/item.model';
import { Extension } from '../core/models/fhir-questionnaire/item/extension/extension.model';
import { ValueCodeableConcept } from '../core/models/fhir-questionnaire/item/value/value-codeable-concept.model';
import { ValueCoding } from '../core/models/fhir-questionnaire/item/value/value-coding.model';


/**
 * RAI QUESTIONS
 * 
 * see: https://lforms-formbuilder.nlm.nih.gov/
 *      https://medium.com/@ahryman40k/handle-fhir-objects-in-typescript-and-javascript-7110f5a0686f
 */
export const RAIQuestionnaire = new Questions({

  id: 'rai-questions',
  date: moment.utc().format('YYYY-MM-DDTHH:mm:ssZ'),
  name: 'RAI-Questionnaire',
  title: 'Pediatric Renal Angina Index (RAI)',
  description: 'Predict acute kidney injury in critically ill children',
  item: [
    new Item({
      linkId: '1',
      text: 'Admitted to ICU?',
      type: 'boolean'
    }),
    new Item({
      linkId: '2',
      text: 'Stem Cell Transplantation?',
      type: 'boolean'
    }),
    new Item({
      linkId: '3',
      text: 'Requiring Ventilator and/or Inotropic Support?',
      type: 'boolean'
    }),
    new Item({
      linkId: '4',
      type: 'choice',
      extension: [
        new Extension({
          url: "http://hl7.org/fhir/StructureDefinition/questionnaire-itemControl",
          valueCodeableConcept: new ValueCodeableConcept({
            coding: [
              new ValueCoding({
                code: 'http://hl7.org/fhir/questionnaire-item-control',
                display: 'drop-down',
                system: 'Drop down',
              }),
            ],
            text: 'Decline in eCrCl?',
          }),
        })
      ],
      answerOption: [
        new ValueCoding({
          code: '0',
          display: 'No change'
        }),
        new ValueCoding({
          code: '1',
          display: '0-25% decline'
        }),
        new ValueCoding({
          code: '2',
          display: '25-50% decline'
        }),
        new ValueCoding({
          code: '3',
          display: '>=50% decline'
        }),
      ]
    }),
    new Item({
      linkId: '5',
      text: 'ICU Admission Weight? (kg)',
      type: 'decimal',
      extension: [
        new Extension({
          url: "http://hl7.org/fhir/StructureDefinition/questionnaire-unit",
          valueCoding: new ValueCoding({
            display: "kg",
          })
        })
      ],
    }),
    new Item({
      linkId: '6',
      text: 'Total Fluid In? (L)',
      type: 'decimal',
      extension: [
        new Extension({
          url: "http://hl7.org/fhir/StructureDefinition/questionnaire-unit",
          valueCoding: new ValueCoding({
            display: "L",
          })
        })
      ],
    }),
    new Item({
      linkId: '7',
      text: 'Total Fluid Out? (L)',
      type: 'decimal',
      extension: [
        new Extension({
          url: "http://hl7.org/fhir/StructureDefinition/questionnaire-unit",
          valueCoding: new ValueCoding({
            display: "L",
          })
        })
      ],
    }),
  ]
});