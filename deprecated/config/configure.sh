#!/bin/bash
#
# NGINX SETUP
#
# see: https://www.digitalocean.com/community/tutorials/how-to-install-nginx-on-ubuntu-18-04
#
sudo rm /etc/nginx/sites-enabled/raicds.com;
sudo rm /etc/nginx/sites-available/raicds.com;

sudo rm -r /var/www/cds-client;
sudo cp -r ../dist/cds-client /var/www/cds-client;

sudo cp nginx.conf /etc/nginx/sites-available/raicds.com;
sudo ln -s /etc/nginx/sites-available/raicds.com /etc/nginx/sites-enabled/raicds.com;
sudo nginx -t;
sudo systemctl restart nginx;


#
# PM2 SETUP
#
# > sudo npm install pm2 -g
#
# see: http://pm2.keymetrics.io/docs/usage/startup/

# client   @localhost:3000
pm2 delete "cds-client";
pm2 start "ecosystem.config.js" --name "cds-client";

# generate startup script
pm2 startup systemd;
sudo env PATH=$PATH:/usr/bin $(which "pm2") startup systemd -u $(whoami) --hp /home/$(whoami);
pm2 save;

