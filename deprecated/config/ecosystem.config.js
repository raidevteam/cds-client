module.exports = {
  apps: [
    {
      name: "cds-client",
      cwd: "/var/www/cds-client",
      args: "-p 3000 -d false",
      script: "/usr/local/bin/http-server"
    }
  ]
};
