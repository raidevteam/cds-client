import { Model, prop } from '@rawmodel/core';
import { ValueCodeableConcept } from '../value/value-codeable-concept.model';
import { ValueCoding } from '../value/value-coding.model';


/**
 * QUESTIONNAIRE ITEM ANSWER EXTENSION MODEL
 */
export class Extension extends Model {

    @prop()
    public url: boolean;

    @prop()
    public valueCodeableConcept: ValueCodeableConcept;

    @prop()
    public valueCoding: ValueCoding;
}