import { Model, prop } from '@rawmodel/core';


/**
 * QUESTIONNAIRE ITEM ANSWER MODEL
 */
export class ItemAnswer extends Model {

    @prop()
    public valueString: string;

    @prop()
    public valueDecimal: number;


}