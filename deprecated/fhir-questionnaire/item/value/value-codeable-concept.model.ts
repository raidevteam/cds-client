import { Model, prop } from '@rawmodel/core';
import { ValueCoding } from './value-coding.model';


/**
 * QUESTIONNAIRE ITEM VALUE CODABLE CONCEPT MODEL
 */
export class ValueCodeableConcept extends Model {

    @prop()
    // e.g. "coding": [
    //     {
    //       "system": "http://hl7.org/fhir/questionnaire-item-control",
    //       "code": "drop-down",
    //       "display": "Drop down"
    //     }
    //   ],
    public coding: ValueCoding[];

    @prop()
    public text: number; // e.g. "Drop Down"
}