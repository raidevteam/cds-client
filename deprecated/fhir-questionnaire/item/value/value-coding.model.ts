import { Model, prop } from '@rawmodel/core';


/**
 * QUESTIONNAIRE ITEM VALUE CODING MODEL
 */
export class ValueCoding extends Model {

    @prop()
    public code: string;    // e.g. "3"

    @prop()
    public display: string; // e.g. ">=50% decline"

    @prop()
    public system: string; // e.g. "http://hl7.org/fhir/questionnaire-item-control"
}