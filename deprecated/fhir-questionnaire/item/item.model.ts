import { Model, prop } from '@rawmodel/core';
import { ItemAnswer } from './answer.model';
import { ValueCoding } from './value/value-coding.model';
import { Extension } from './extension/extension.model';


/**
 * QUESTIONNAIRE ITEM MODEL
 * 
 * see: https://www.hl7.org/fhir/valueset-item-type.html
 */
export class Item extends Model {

    @prop()
    public linkId: string;

    @prop()
    public text: string;    // e.g. 'Admitted to ICU?'

    @prop()
    public extension: Extension[];

    @prop({ defaultValue: "string" })
    public type: string;    // e.g. 'string' or 'decimal'

    @prop({ defaultValue: false })
    public required: boolean;

    @prop()
    public answer: ItemAnswer[];

    @prop()
    public answerOption: ValueCoding[];
}