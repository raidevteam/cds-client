import { Model, prop } from '@rawmodel/core';
import { Item } from './item/item.model';


/**
 * QUESTIONNAIRE RESPONSE MODEL
 * 
 * see: https://www.hl7.org/fhir/questionnaireresponse.html
 */
export class Response extends Model {

    @prop()
    public id: string;

    @prop({ defaultValue: 'QuestionnaireResponse' })
    public resourceType: string;

    @prop({ defaultValue: 'in-progress' })
    // see: https://www.hl7.org/fhir/valueset-questionnaire-answers-status.html
    public status: string; // e.g. 'in-progress' or 'completed'

    @prop()
    public authored: string;

    @prop()
    public item: Item[];
}
