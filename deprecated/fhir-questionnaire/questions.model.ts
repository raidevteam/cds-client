import { Model, prop } from '@rawmodel/core';
import { Item } from './item/item.model';


/**
 * QUESTIONNAIRE QUESTIONS MODEL
 * 
 * see: https://www.hl7.org/fhir/questionnaire.html
 */
export class Questions extends Model {

    @prop()
    public id: string;

    @prop({ defaultValue: 'Questionnaire' })
    public resourceType: string;

    @prop({ defaultValue: 'active' })
    public status: string;

    @prop({ defaultValue: 'patient' })
    public subjectType: string;

    @prop()
    public name: string;    // name for this questionnaire (computer friendly)

    @prop()
    public title: string;   // name for this questionnaire (human friendly)

    @prop()
    public description: string;

    @prop()
    public date: string;    // datetime format: YYYY-MM-DDThh:mm:ss+zz:zz

    @prop()
    public item: Item[];
}