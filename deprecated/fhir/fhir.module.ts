import { ModuleWithProviders, NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
// fhir
import { FHIR_HTTP_CONFIG, NgFhirjsModule } from 'ng-fhirjs'
import { FHIR_JS_CONFIG } from '../shared/consts';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    // fhir
    NgFhirjsModule
  ]
})
export class FhirModule {

  constructor(@Optional() @SkipSelf() parentModule?: FhirModule) {
    if (parentModule) {
      throw new Error(
        'FhirModule is already loaded. Import it in the AppModule only');
    }
  }

  public static forRoot(): ModuleWithProviders<FhirModule> {
    return {
      ngModule: FhirModule,
      providers: [{ provide: FHIR_HTTP_CONFIG, useValue: FHIR_JS_CONFIG }]
    };
  }
}
