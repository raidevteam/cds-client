## Developers Notes
# Add bootstrap to cli project
> ng add @ng-bootstrap/schematics

# SMART App Launch Framework
see: http://hl7.org/fhir/smart-app-launch/

# Registering a SMART App with an EHR
* We will register our app with the EHR's authorization service e.g. OAuth 2.0 Dynamic Client Registration Protocol.

1. SMART Launch sequence
   We need to support the following two approuches 
   * "EHR launch" and 
   * "standalone". 
   In each case, we must complete the following steps:
   * Initiate HTTP requests to retreive a "metadata" file from the EHR FHIR server
   * Use the "metadata" to request "scopes"/"authorization" from the EHR Authz server

2. SMART authorization and resource retrieval
   Request authorization
   * We navigate to an EHR hosted authorization page
   * We handle a redirect URL and extract "client_access"
