import { Component, Input } from '@angular/core';
// mdb
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout'
import { Router } from '@angular/router';


@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {

  @Input() recentPatients?: fhir.Patient[];
  fullTitle: string;
  condTitle: string;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private router: Router
  ) {
    // set defaults
    this.fullTitle = 'Renal Angina Index (RAI) - Predict acute kidney injury risk within the next 72 hours';
    this.condTitle = 'Renal Angina Index (RAI) ⓘ';
  }


  get title(): string {
    return this.isSmallScreen ? this.condTitle : this.fullTitle;
  }

  get isSmallScreen(): boolean {
    return this.breakpointObserver.isMatched([Breakpoints.XSmall, Breakpoints.Small]);
  }

  patientName(patient: fhir.Patient) {
    if (!patient) { return; }
    const name = (<fhir.HumanName>patient.name)[0];
    const firstName = name.given[0];
    const lastName = name.family;
    return `${firstName} ${lastName}`;
  }

  patientLink(patient: fhir.Patient) {
    return `/patient?id=${patient.id}`;
  }

}

//--------------------------------

// ngOnChanges(changes: SimpleChanges): void {
//   // select first patient on patient list change
//   if (changes['recentPatients'] && this.recentPatients) {
//     const url = '/patient';
//     // const url = this.patientLink(this.recentPatients[0]);
//     this.router.navigate([url]);
//   }
// }