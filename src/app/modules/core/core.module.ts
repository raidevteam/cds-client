import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
// bootstrap
import { MDBBootstrapModule } from 'angular-bootstrap-md';
// rai
import { HeaderComponent } from './components/header/header.component';



@NgModule({
  declarations: [
    HeaderComponent,
  ],
  imports: [
    // vender
    CommonModule,
    // mdb
    MDBBootstrapModule.forRoot()
  ],
  exports: [
    HeaderComponent,
  ]
})
export class CoreModule {
  constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
    if (parentModule) {
      throw new Error(
        'CoreModule is already loaded. Import it in the AppModule only'
      );
    }
  }
}
