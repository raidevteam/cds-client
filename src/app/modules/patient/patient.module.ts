import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PatientRoutingModule } from './patient-routing.module';
import { PatientComponent } from './patient.component';
import { SharedModule } from '../shared/shared.module';
import { PatientHeaderComponent } from './components/patient-header/patient-header.component';
import { QuestModule } from '../quest/quest.module';


@NgModule({
  declarations: [
    PatientComponent,
    PatientHeaderComponent
  ],
  imports: [
    // vendor
    CommonModule,
    PatientRoutingModule,
    // rai
    SharedModule,
    QuestModule
  ]
})
export class PatientModule { }
