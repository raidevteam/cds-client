import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
// rxjs
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
// rai
import { PatientService } from '../shared/services/patient/patient.service';


@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.scss']
})
export class PatientComponent implements OnInit, OnDestroy {

  public patient?: fhir.Patient;
  private _routeSub$?: Subscription;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private patientService: PatientService,
  ) { }

  async ngOnInit() {
    // set patient ID from URL
    const params = await this.route.queryParams.pipe(first()).toPromise();
    if(params === undefined) { return; }

    // set default patient ID if undefined
    const patientID = params.id ? params.id : 'SMART-7321938';
    this.router.navigate([''], { queryParams: { id: patientID } });
    this.patient = await this.patientService.getPatientByID(patientID);
  }

  ngOnDestroy(): void {
    // remove subscriptions
    if (this._routeSub$) { this._routeSub$.unsubscribe(); }
  }

}
