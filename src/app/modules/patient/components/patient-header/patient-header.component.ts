import { Component, Input } from '@angular/core';

@Component({
  selector: 'app-patient-header',
  templateUrl: './patient-header.component.html',
  styleUrls: ['./patient-header.component.scss']
})
export class PatientHeaderComponent {

  @Input() patient?: fhir.Patient;

  patientName(patient: fhir.Patient) {
    if (!patient) { return; }
    const name = (<fhir.HumanName>patient.name)[0];
    const firstName = name.given[0];
    const lastName = name.family;
    return `${firstName} ${lastName}`;
  }

}
