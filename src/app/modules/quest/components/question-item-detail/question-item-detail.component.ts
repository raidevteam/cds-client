import { Component, Input } from '@angular/core';
import * as moment from 'moment';
// rai
import { ConfidenceType } from '../../../shared/enums';
import { Question } from '../../../shared/models/question.model';


@Component({
  selector: 'app-question-item-detail',
  templateUrl: './question-item-detail.component.html',
  styleUrls: ['./question-item-detail.component.scss']
})
export class QuestionItemDetailComponent {

  @Input() question?: Question;

  lastModifiedDateTime(): any {
    // format date e.g. Fri Oct 09 2020 17:42 CDT
    const timeZone = new Date()
      .toLocaleTimeString('en-us', { timeZoneName: 'short' })
      .split(' ')[2];
    if(!this.question) { return; }
    const dateTime = moment(this.question.lastModified)
      .format('MMM Do YYYY, HH:mm').toString();
    return `${dateTime} ${timeZone}`;
  }

  confidenceColorClass(): any {
    if(!this.question) { return; }
    switch (this.question.confidence) {
      case ConfidenceType.High: return { 'btn-confidence-high': true };
      case ConfidenceType.Medium: return { 'btn-confidence-medium': true };
      case ConfidenceType.Low: return { 'btn-confidence-low': true };
    }
  }

  confidenceIconName(): string | undefined {
    if(!this.question) { return; }
    switch (this.question.confidence) {
      case ConfidenceType.High: return 'check-circle-fill';
      case ConfidenceType.Medium: return 'exclamation-triangle-fill';
      case ConfidenceType.Low: return 'exclamation-octagon-fill';
    }
  }

}


// ----------------------------------------------

// lastModifiedHoursAgo(): number {
//   const elapsedMsec = new Date().getTime() - this.question.lastModified.getTime();
//   return Math.floor(elapsedMsec / 3600000);
// }
