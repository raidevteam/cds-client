import { ComponentFixture, TestBed } from '@angular/core/testing';

import { QuestionItemDetailComponent } from './question-item-detail.component';

describe('QuestionItemDetailComponent', () => {
  let component: QuestionItemDetailComponent;
  let fixture: ComponentFixture<QuestionItemDetailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ QuestionItemDetailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(QuestionItemDetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
