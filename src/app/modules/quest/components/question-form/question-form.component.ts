import { Component, OnInit, Input, ViewChild, OnDestroy, Output, EventEmitter } from '@angular/core';
import { NgForm } from '@angular/forms';
// rxjs
import { debounceTime } from 'rxjs/operators';
// rai
import { Subscription } from 'rxjs';
import { Question } from '../../../shared/models/question.model';
import { Quest } from '../../../shared/models/quest.model';

/**
 * QUESTION FORM COMPONENT
 * 
 * see: https://www.bytefish.de/blog/composable_surveys_angular_9.html
 */
@Component({
  selector: 'app-question-form',
  templateUrl: './question-form.component.html',
  styleUrls: ['./question-form.component.scss']
})
export class QuestionFormComponent implements OnInit, OnDestroy {

  @Input() questionnaire?: Quest;
  @ViewChild('form') ngForm?: NgForm;

  @Output() formChange = new EventEmitter<Quest>();
  private _formChangesSubscription?: Subscription;

  ngOnInit(): void { }

  ngDoCheck(): void {
    // listen for changes
    if (!this.ngForm || this._formChangesSubscription) { return; }

    // update questionnaire items
    this._formChangesSubscription = this.ngForm.form.valueChanges
      .pipe(debounceTime(250))
      .subscribe(formValues => {
        this._handleQuestionnaireChange(formValues);
      });
  }

  ngOnDestroy() {
    if(this._formChangesSubscription) {
      this._formChangesSubscription.unsubscribe();
    }
  }

  isQuestionTypeLabel(question: Question) {
    return (question.type === 'label') ? true : false;
  }

  /**
   * HELPER
   */
  private _handleQuestionnaireChange(formValues: any[]) {
    try {
      if(!this.questionnaire) { return;}
      this.questionnaire.items = this._updateQuestionnaireItems(this.questionnaire, formValues);
      this.formChange.emit(this.questionnaire);
    } catch (err) {
      console.log(`QuestionFormComponent: failed to handle form change, ${err}`);
    }
  }

  private _updateQuestionnaireItems(questionnaire: Quest, formValues: any[]) {
    const questions = questionnaire.items;
    for (const [idx, [key, value]] of Object.entries(formValues).entries()) {
      if (key !== questions[idx].id) { throw `invalid survey response`; }
      questionnaire.items[idx].value = value['formResponse'] as any;
    }
    return questionnaire.items;
  }

}

//-----------------------------------------

// onSubmit(form: NgForm): void {

//   const responses = form.value;
//   const questions = this.questionnaire.items;

//   for (const [idx, [key, value]] of Object.entries(responses).entries()) {
//     if (key !== questions[idx].id) { throw `invalid survey response`; }
//     this.questionnaire.items[idx].value = value['formResponse'] as any;
//   }
//   this.formChange.emit(this.questionnaire);
// }