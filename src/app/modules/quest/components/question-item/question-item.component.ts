import { AfterViewInit, ChangeDetectorRef, Component, Input, SkipSelf } from '@angular/core';
import { ControlContainer } from '@angular/forms';
import { Question } from '../../../shared/models/question.model';

@Component({
  selector: 'app-question-item',
  templateUrl: './question-item.component.html',
  styleUrls: ['./question-item.component.scss'],
  viewProviders: [{
    provide: ControlContainer,
    useFactory: (container: ControlContainer) => container,
    deps: [[new SkipSelf(), ControlContainer]],
  }]
})
export class QuestionItemComponent implements AfterViewInit {

  @Input() modelGroupName?: string;
  @Input() question?: Question;

  constructor(private cdRef: ChangeDetectorRef) { }

  ngAfterViewInit(): void {
    this.cdRef.detectChanges();
  }


}
