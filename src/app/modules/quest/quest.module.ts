import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SharedModule } from '../shared/shared.module';

import { FormsModule } from '@angular/forms';

import { QuestComponent } from './quest.component';
import { QuestionItemComponent } from './components/question-item/question-item.component';
import { QuestionItemDetailComponent } from './components/question-item-detail/question-item-detail.component';
import { QuestionFormComponent } from './components/question-form/question-form.component';
import { QuestRoutingModule } from './quest-routing.module';


/**
 * QUESTIONNAIRE MODULE
 * 
 * Questionnaire vs Survey - A questionnaire is often described as 
 * a collection of questions designed to collect information from 
 * participants. When we refer to a questionnaire we are talking about 
 * the questions and the potential answers within a survey. 
 * see: https://www.dragnsurvey.com/blog/en/the-difference-between-survey-and-questionnaire/.
 * 
 * In this case, we will collect data using two seperate approuches
 * 1) a questionnaire and 2) an Electronic Health Record (EHR).
 * 
 * We will pre-populate the questionnaire with data fields from the EHR.
 */
@NgModule({
  declarations: [
    QuestComponent,
    QuestionItemComponent,
    QuestionItemDetailComponent,
    QuestionFormComponent,
  ],
  imports: [
    // vendor
    CommonModule,
    FormsModule,
    // rai
    SharedModule,
    QuestRoutingModule,
  ],
  exports: [
    QuestComponent,
  ]
})
export class QuestModule { }
