import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { DefaultQuestionnaire, Quest } from '../shared/models/quest.model';
import { PatientService } from '../shared/services/patient/patient.service';


@Component({
  selector: 'quest-page',
  templateUrl: './quest.component.html',
  styleUrls: ['./quest.component.scss']
})
export class QuestComponent implements OnChanges {

  @Input() patient?: fhir.Patient;
  public questionnaire?: Quest;
  public score?: number;

  constructor(private patientService: PatientService) { }

  async ngOnChanges(changes: SimpleChanges) {
    if (changes['patient']) {
      this.questionnaire = await this.handleOnPatientChange(this.patient);
      this.score = Quest.calculateRAIScore(this.questionnaire);
    }
  }

  async handleOnPatientChange(patient: fhir.Patient | undefined) {
    try {
      if (!patient) { return; }

      const encounters = await this.patientService.getPatientEncounters(this.patient);
      const observations = await this.patientService.getPatientObservations(this.patient);
      const procedures = await this.patientService.getPatientProcedures(this.patient);

      const defaultItems = DefaultQuestionnaire.items;

      // 'Q1: 'Admitted to ICU?'
      defaultItems[0].value = this._isICUPatient(encounters);

      // 'Q2: Stem Cell Transplantation or solid organ?',
      defaultItems[1].value = this._isTransplantPatient(procedures);

      // 'Q3: Requiring Ventilator and/or Inotropic Support?'
      defaultItems[2].value = this._isVentilatorPatient(procedures);

      // 'Q4: Baseline Creatinine (Cr)'
      defaultItems[3].value = this._baselineCreatinine(observations);

      // 'Q5: Current Creatinine (Cr)'
      defaultItems[4].value = this._currentCreatinine(observations);

      // 'Q6: ICU Admission Weight?'
      defaultItems[5].value = this._bodyWeight(observations);

      // 'Q7: Total Fluid In?'
      defaultItems[6].value = this._fluidIntake(observations);

      // 'Q8: Total Fluid Out?'
      defaultItems[7].value = this._fluidOutput(observations);

      const questionnaire = DefaultQuestionnaire;
      questionnaire.items = defaultItems;
      return questionnaire;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to handle patient change, ${err}`);
    }
  }

  handleOnFormChange(questionnaire: Quest): void {
    try {
      if (!questionnaire) { return; }
      this.questionnaire = questionnaire;
      this.score = Quest.calculateRAIScore(questionnaire) as number;
      console.log(`QuestionnaireComponent: updated RAI score, ${this.score}`);

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to handle on form change, ${err}`);
    }
  }

  /**
   * HELPERS
   */
  private _isICUPatient(encounters: fhir.Encounter[] | undefined): boolean | undefined {
    try {
      if (!encounters) { return; }
      for (const encounter of encounters) {
        if(!encounter || encounter.class === undefined) { continue; }
        if (encounter.class.code === 'ICU') { return true; }
      }
      return false;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check ICU status, ${err}`);
    }
  }

  private _isTransplantPatient(procedures: fhir.Procedure[] | undefined): boolean | undefined {
    try {
      if (!procedures) { return; }
      for (const procedure of procedures) {
        if(!procedure || procedure.code === undefined) { continue; }
        const coding = (<fhir.Coding>procedure.code.coding)[0];
        if (coding.system === 'SNOMED'
          && coding.code === '234336002') { return true; }
      }
      return false;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check transplant status, ${err}`);
    }
  }

  private _isVentilatorPatient(procedures: fhir.Procedure[] | undefined): boolean | undefined {
    try {
      if (!procedures) { return; }
      for (const procedure of procedures) {
        if(!procedure || procedure.code === undefined) { continue; }
        const coding = (<fhir.Coding>procedure.code.coding)[0];
        if (coding.system === 'SNOMED'
          && coding.code === '410210009') { return true; }
      }
      return false;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check ventilator status, ${err}`);
    }
  }

  private _fluidIntake(observations: fhir.Observation[] | undefined): number | undefined {
    try {
      if (!observations) { return; }
      for (const observation of observations) {
        const coding = (<fhir.Coding>observation.code.coding)[0];
        if (coding.system === 'LOINC'
          && coding.code === '9103-3') {
          const liters = observation.valueQuantity ? observation.valueQuantity.value : undefined;
          return liters;
        }
      }
      return undefined;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check fluid intake, ${err}`);
    }
  }

  private _fluidOutput(observations: fhir.Observation[] | undefined): number | undefined {
    try {
      if (!observations) { return; }
      for (const observation of observations) {
        const coding = (<fhir.Coding>observation.code.coding)[0];
        if (coding.system === 'LOINC'
          && coding.code === '9257-7') {
          const liters = observation.valueQuantity ? observation.valueQuantity.value : undefined;
          return liters;
        }
      }
      return undefined;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check fluid output, ${err}`);
    }
  }

  private _bodyWeight(observations: fhir.Observation[] | undefined): number | undefined  {
    try {
      if (!observations) { return; }
      for (const observation of observations) {
        const coding = (<fhir.Coding>observation.code.coding)[0];
        if (coding.system === 'LOINC'
          && coding.code === '29463-7') {
          const kgs = observation.valueQuantity ? observation.valueQuantity.value : undefined;
          return kgs;
        }
      }
      return undefined;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check body weight, ${err}`);
    }
  }

  private _baselineCreatinine(observations: fhir.Observation[] | undefined): number | undefined {
    try {
      if (!observations) { return; }
      for (const observation of observations) {
        const coding = (<fhir.Coding>observation.code.coding)[0];
        if (coding.display === 'Baseline Creatinine') {
          const mgPerDl = observation.valueQuantity ? observation.valueQuantity.value : undefined;
          return mgPerDl;
        }
      }
      return undefined;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check baseline creatine, ${err}`);
    }
  }

  private _currentCreatinine(observations: fhir.Observation[] | undefined): number | undefined {
    try {
      if (!observations) { return; }
      for (const observation of observations) {
        const coding = (<fhir.Coding>observation.code.coding)[0];
        if (coding.display === 'Current Creatinine') {
          const mgPerDl = observation.valueQuantity ? observation.valueQuantity.value : undefined;
          return mgPerDl;
        }
      }
      return undefined;

    } catch (err) {
      console.log(`QuestionnaireComponent: failed to check current creatine, ${err}`);
    }
  }



}



// ---------------------------

// constructor(private storageService: StorageService) { }

// ngOnInit(): void {
//   // set defaults
//   if (CLEAR_CACHE) {
//     this.storageService.remove(SampleMRN);
//   }
//   let questionnaire = this.storageService.get(SampleMRN);
//   if (!questionnaire) {
//     this.storageService.set(SampleMRN, new Questionnaire(SampleQuestionnaire));
//     questionnaire = this.storageService.get(SampleMRN);
//   }
//   this.questionnaire = questionnaire;
//   this.handleOnFormChange(this.questionnaire);
// }