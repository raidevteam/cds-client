/**
 * ENUMS
 */

export enum ConfidenceType {
    High = 'high',
    Medium = 'medium',
    Low = 'low',
}