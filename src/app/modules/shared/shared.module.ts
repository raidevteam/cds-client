import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
// bootstrap
import { MDBBootstrapModule } from 'angular-bootstrap-md';
// rai
// import { FhirModule } from '../fhir/fhir.module';


/**
 * SHARED MODULE
 * 
 * Shared declarations, pipes and components among seperate modules
 * 
 * Note: Most 'services' will be used as global singletons rather than
 * having seperate services instances within each module. Impliment global
 * singleton services in the '/shared' folder with the 'providedIn: 'root' 
 * syntax and add module-specific services using the 'providers: [ ]' syntax. 
 * see: https://medium.com/@tomastrajan/how-to-build-epic-angular-app-with-clean-architecture-91640ed1656
 */
@NgModule({
  declarations: [],
  imports: [
    // vendor
    CommonModule,
    // mdb
    MDBBootstrapModule.forRoot()
  ],
  exports: [
    CommonModule,
    MDBBootstrapModule,
  ]
})
export class SharedModule { }
