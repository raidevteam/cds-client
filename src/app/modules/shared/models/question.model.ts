import { ConfidenceType } from '../enums';


/**
 * QUESTIONNAIRE QUESTION MODEL
 */
export class Question {
    public id?: string;
    public text?: string;
    public value?: string | boolean | number | undefined;
    public units?: string;
    public lastModified?: Date;
    public type?: string;
    public choices?: string[];
    public source?: string;
    public confidence?: ConfidenceType;
    public tooltip?: string;

    constructor(data?: Partial<Question>) {
        Object.assign(this, JSON.parse(JSON.stringify(data)));
    }
}