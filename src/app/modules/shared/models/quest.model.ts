import { utc } from 'moment';
import { ConfidenceType } from '../enums';
import { Question } from './question.model';


/**
 * QUESTIONNAIRE MODEL
 */
export class Quest {

    public id?: string;
    public name?: string;    // name for this questionnaire (computer friendly)
    public title?: string;   // name for this questionnaire (human friendly)
    public description?: string;
    public date?: string;      // last modified, UTC 'YYYY-MM-DDTHH:mm:ssZ'
    public items: Question[] = [];

    constructor(data?: Partial<Quest>) {
        Object.assign(this, JSON.parse(JSON.stringify(data)));
    }

    /**
     * Calculate RAI Score
     * 
     * @param questionnaire 
     */
    public static calculateRAIScore(questionnaire: Quest | undefined) {
        // see: https://reader.elsevier.com/reader/sd/pii/S2352464217301815?token=DE4D42EAF0B998B9CBBDB67EAEA28328B9C69AD5E291F48839A1CA42A428ED67318E39CED76385A1861B69740CFEB314
        //      https://qxmd.com/calculate/calculator_361/pediatric-renal-angina-index
        try {
            if (!questionnaire) { return; }
            const items = questionnaire.items;
            if (!items || items.length === 0) { return; }

            const itemIDToValue = new Map(
                items.map(item => [item.id, item.value])
            );
            const riskScore = this._riskScore(
                itemIDToValue.get('Q1') as boolean,
                itemIDToValue.get('Q2') as boolean,
                itemIDToValue.get('Q3') as boolean,
            );
            const injuryScore = this._injuryScore(
                itemIDToValue.get('Q7') as number,
                itemIDToValue.get('Q8') as number,
                itemIDToValue.get('Q6') as number,
                itemIDToValue.get('Q5') as number,
                itemIDToValue.get('Q4') as number,
            );
            if(riskScore === undefined || injuryScore === undefined) { 
                throw `risk or injury score undefined`; 
            }
            const raiScore = riskScore * injuryScore;
            return raiScore;

        } catch (err) {
            console.log(`Questionnaire: failed to calculate RAI score, ${err}`);
        }
    }

    /**
     * HELPERS
     */
    /**
     *  Risk score
     * 
     * @param isICUAdmission 
     * @param hasStemTransplant 
     * @param isUsingVentilator 
     */
    private static _riskScore(isICUAdmission: boolean, hasStemTransplant: boolean, isUsingVentilator): number | undefined  {
        // ICU admission alone: Moderate risk with a score of 1
        // Stem cell transplantation: High risk with a score of 3
        // Ventilation or intropes: Very high risk with a score of 5
        try {
            if (isUsingVentilator) { return 5; }
            if (hasStemTransplant) { return 3; }
            if (isICUAdmission) { return 1; }

            return 0;

        } catch (err) {
            console.log(`Questionnaire: failed to calculate injury score, ${err}`);
        }
    }

    /**
     * Injury score
     * 
     * @param fluidIn (L)
     * @param fluidOut (L)
     * @param weight ICU admission weight (kg)
     * @param baselineECrCl creatinine clearance (eCrCl), (ml/min)
     * @param currentECrCL creatinine clearance (eCrCl), (ml/min)
     */
    private static _injuryScore(fluidIn: number, fluidOut: number, weight: number, currentECrCL: number, baselineECrCl: number = 1.73): number | undefined {
        // Clinical signs of injury are based on changes in estimated
        // creatinine clearance (eCrCl) or % fluid overload (%FO).
        //
        // The point values for injury are:
        // 1 - No decrease in eCrCl or <5% FO
        // 2 - >5% FO or eCrCl decrease of 0-25%
        // 4 - >10% FO or eCrCl decrease of 25-50%
        // 8 - >15% FO or eCrCl decrease of >50%
        //
        // see: https://qxmd.com/calculate/calculator_361/pediatric-renal-angina-index
        try {
            // (fluid in [L] - fluid out [L])/(ICU admit weight [kg]) x 100%
            // CONFLICT: "fluid overload wasdivided into 3 severity strata for univariate analyses"
            const fluidOverload = (fluidIn - fluidOut) / weight * 100.0;

            // lowest SCr up to 3 months before PICU admission to 
            // establish a reference eCcCl
            const pctDecreaseECrCL = (baselineECrCl - currentECrCL) / baselineECrCl * 100.0;

            const injuryPts: number = fluidOverload > 15 || pctDecreaseECrCL > 50 ? 8
                : fluidOverload > 10 || pctDecreaseECrCL > 25 ? 4
                    : fluidOverload > 5 || pctDecreaseECrCL > 0 ? 2
                        : fluidOverload > 0 || pctDecreaseECrCL <= 0 ? 1 : 0;

            return injuryPts;

        } catch (err) {
            console.log(`Questionnaire: failed to calculate injury score, ${err}`);
        }
    }
}

/**
     * DEFAULT QUESTIONNAIRE
     * 
     * see: https://lforms-formbuilder.nlm.nih.gov/
     *      https://medium.com/@ahryman40k/handle-fhir-objects-in-typescript-and-javascript-7110f5a0686f
     */
export const DefaultQuestionnaire = new Quest({
    id: 'rai',
    date: utc().format('YYYY-MM-DDTHH:mm:ssZ'),
    name: 'RAI-Questionnaire',
    title: 'Pediatric Renal Angina Index (RAI)',
    description: 'Predict acute kidney injury in critically ill children',
    items: [
        new Question({
            id: 'Q1',
            text: 'Admitted to ICU?',
            type: 'boolean',
            value: undefined,
            source: "EHR",
            lastModified: new Date(),
            confidence: ConfidenceType.High,
        }),
        new Question({
            id: 'Q2',
            text: 'Stem Cell Transplantation or solid organ?',
            type: 'boolean',
            value: undefined,
            source: "EHR",
            lastModified: new Date(),
            confidence: ConfidenceType.High,
        }),
        new Question({
            id: 'Q3',
            text: 'Requiring Ventilator and/or Inotropic Support?',
            type: 'boolean',
            value: undefined,
            source: "EHR",
            lastModified: new Date(),
            confidence: ConfidenceType.High,
        }),
        new Question({
            id: 'Q4',
            type: 'decimal',
            text: 'Baseline Creatinine (Cr)',
            value: 1.73,
            units: "mg/dL",
            source: "EHR",
            lastModified: new Date(new Date().setDate(new Date().getDate() - 2)),
            confidence: ConfidenceType.Medium,
            tooltip: 'lowest within last 3 months'
        }),
        new Question({
            id: 'Q5',
            type: 'decimal',
            text: 'Current Creatinine (Cr)',
            value: undefined,
            units: "mg/dL",
            source: "EHR",
            lastModified: new Date(new Date().setDate(new Date().getDate())),
            confidence: ConfidenceType.Medium,
        }),
        new Question({
            id: 'Q6',
            text: 'ICU Admission Weight?',
            type: 'decimal',
            value: undefined,
            units: "kg",
            source: "EHR",
            lastModified: new Date(),
            confidence: ConfidenceType.High,
        }),
        new Question({
            id: 'Q7',
            text: 'Total Fluid In?',
            type: 'decimal',
            value: undefined,
            units: "L",
            source: "Manual entry",
            lastModified: new Date(),
            confidence: ConfidenceType.Medium,
        }),
        new Question({
            id: 'Q8',
            text: 'Total Fluid Out?',
            type: 'decimal',
            value: undefined,
            units: "L",
            source: "Manual entry",
            lastModified: new Date(),
            confidence: ConfidenceType.Medium,
        }),
    ]
});
