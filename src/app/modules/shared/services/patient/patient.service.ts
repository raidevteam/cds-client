import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { FHIR_CONFIG } from '../../consts';


@Injectable({
  providedIn: 'root'
})
export class PatientService {

  constructor(private http: HttpClient) { }

  async getPatients(): Promise<fhir.Patient[] | undefined> {
    try {
      const url = `${FHIR_CONFIG.baseUrl}/Patient`;
      const bundle = await this.http.get(url, FHIR_CONFIG.options).toPromise() as any;
      if (!bundle || !bundle.entry) { return; }
      const patients = <Array<fhir.Patient>>bundle.entry.map(r => r['resource']);
      return patients;

    } catch (err) {
      console.log(`PatientService: failed to get patients, ${err}`);
    }
  }

  async getPatientByIDs(ids: string[]): Promise<fhir.Patient[] | undefined> {
    try {
      const patients =  await Promise.all(
        ids.map(patientID => this.getPatientByID(patientID))
      );
      return patients as fhir.Patient[];
      
    } catch (err) {
      console.log(`PatientService: failed to get patient by IDs, ${err}`);
    }
  }

  async getPatientByID(id: string): Promise<fhir.Patient | undefined> {
    try {
      const url = `${FHIR_CONFIG.baseUrl}/Patient/${id}`;
      const data = await this.http.get(url, FHIR_CONFIG.options).toPromise() as any;
      const patient = <fhir.Patient>data;
      return patient;

    } catch (err) {
      console.log(`PatientService: failed to get patient by ID, ${err}`);
    }
  }

  async getPatientObservations(patient: fhir.Patient | undefined): Promise<fhir.Observation[] | undefined> {
    try {
      if(!patient) { return; }
      const url = `${FHIR_CONFIG.baseUrl}/Observation?subject=${patient.id}`;
      const bundle = await this.http.get(url, FHIR_CONFIG.options).toPromise() as any;
      if (!bundle || !bundle.entry) { return; }
      const observations = <Array<fhir.Observation>>bundle.entry.map(r => r['resource']);
      return observations;

    } catch (err) {
      console.log(`PatientService: failed to get observations, ${err}`);
    }
  }

  async getPatientEncounters(patient: fhir.Patient | undefined): Promise<fhir.Encounter[] | undefined> {
    try {
      if(!patient) { return; }
      const url = `${FHIR_CONFIG.baseUrl}/Encounter?subject=${patient.id}`;
      const bundle = await this.http.get(url, FHIR_CONFIG.options).toPromise() as any;
      if (!bundle || !bundle.entry) { return; }
      const observations = <Array<fhir.Encounter>>bundle.entry.map(r => r['resource']);
      return observations;

    } catch (err) {
      console.log(`PatientService: failed to get encounters, ${err}`);
    }
  }

  async getPatientProcedures(patient: fhir.Patient | undefined): Promise<fhir.Procedure[] | undefined> {
    try {
      if(!patient) { return; }
      const url = `${FHIR_CONFIG.baseUrl}/Procedure?subject=${patient.id}`;
      const bundle = await this.http.get(url, FHIR_CONFIG.options).toPromise() as any;
      if (!bundle || !bundle.entry) { return; }
      const procedures = <Array<fhir.Procedure>>bundle.entry.map(r => r['resource']);
      return procedures;

    } catch (err) {
      console.log(`PatientService: failed to get procedures, ${err}`);
    }
  }

}
