import { Injectable } from '@angular/core';
import { CookieService } from 'angular2-cookie/core';
import { v4 as uuidv4 } from 'uuid';
import { HttpClient, HttpHeaders } from '@angular/common/http';


@Injectable()
export class AuthService {

  constructor(
    private http: HttpClient,
    private cookieService: CookieService
  ) {
    console.log("SmartService has been initialized.");
  }

  fhirBaseUrl: string;
  authorizeUrl: string;
  tokenUrl: string;
  clientId: string = "82b330f7-1186-4059-8c31-62dce4b18d77";
  launch: string;
  scope: string = "launch patient/*.* openid profile";
  redirectUri: string = "http://patient-viewer.healthcreek.org";
  state: string;
  // aud: string;

  async authenticate() {
    const fhirBaseUrl = this._findGetParameter("iss");
    if (fhirBaseUrl) {
      // set SMART 'launch' params
      this.fhirBaseUrl = fhirBaseUrl;
      this.launch = this._findGetParameter("launch");
      const metadata = await this.requestMeta(fhirBaseUrl);
      if (!metadata) { throw `metadata undefined`; }
      this.authorizeUrl = metadata.rest[0].security.extension[0].extension[0].valueUri;
      this.tokenUrl = metadata.rest[0].security.extension[0].extension[1].valueUri;
      this.cookieService.put('tokenUrl', this.tokenUrl);
      this.cookieService.put('fhirBaseUrl', this.fhirBaseUrl);

      // navigate to SMART authentication page
      this.state = uuidv4();
      this.cookieService.put('state', this.state);
      const request = this.authorizeUrl + "?response_type=code"
        + "&client_id=" + this.clientId
        + "&redirect_uri=" + this.redirectUri
        + "&launch=" + this.launch
        + "&scope=" + this.scope
        + "&state=" + this.state;
      // + "&aud=" + this.aud;
      window.location.href = encodeURI(request);
    }
    else {
      if (this.cookieService.get('state') === this._findGetParameter('state')) {
        return await this.getToken();
      }
      else {
        console.log('Stop cross-site scripting please, thanks');
      }
    }
  }

  async getToken(): Promise<any> {
    // request 'token' from the EHR Authz server
    const code = this._findGetParameter("code");
    const body = `code=${code}&redirect_uri=${encodeURI(this.redirectUri)}&token_url=${this.cookieService.get('tokenUrl')}`;
    const options = { headers: new HttpHeaders({ 'Content-Type': 'application/x-www-form-urlencoded' }) };
    const token = await this.http.post("http://mongo-proxy.healthcreek.org/token", body, options).toPromise();
    return token;
  }

  async requestMeta(fhirBaseUrl: string): Promise<any> {
    // request 'metadata' from the EHR FHIR server
    const url = `${fhirBaseUrl}/metadata`;
    const options = { headers: new HttpHeaders({ 'Accept': 'application/json' }) };
    const metadata = await this.http.get(url, options).toPromise() as any;
    return metadata;
  }

  /**
   * HELPERS
   */
  private _findGetParameter(parameterName) {
    let result = null;
    let tmp = [];
    location.search
      .substr(1)
      .split("&")
      .forEach(function (item) {
        tmp = item.split("=");
        if (tmp[0] === parameterName) {
          result = decodeURIComponent(tmp[1]);
        }
      });
    return result;
  }

}

