import { HttpHeaders } from '@angular/common/http';

export const FHIR_CONFIG = {
    baseUrl: 'https://api.logicahealth.org/RAICDS/open',
    options: { headers: new HttpHeaders({ 'Accept': 'application/json' }) }
};
export const FHIR_DEMO_PATIENTS_ENABLED = true;
export const FHIR_DEMO_PATIENT_IDS = [
    'SMART-7321938',
    'SMART-2004454',
    'SMART-7777703',
    'SMART-7777705'
];

// Billie Himston
// Kevin Lee
// Paul Luttrell
// Angela Montgomery