import { Component, OnInit } from '@angular/core';
import { FHIR_DEMO_PATIENTS_ENABLED, FHIR_DEMO_PATIENT_IDS } from './modules/shared/consts';
import { PatientService } from './modules/shared/services/patient/patient.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  recentPatients?: fhir.Patient[];

  constructor(private patientService: PatientService) { }

  async ngOnInit() {
    this.recentPatients = FHIR_DEMO_PATIENTS_ENABLED
      ? await this.patientService.getPatientByIDs(FHIR_DEMO_PATIENT_IDS)
      : await this.patientService.getPatients();
    // console.log(`AppComponent: fetched patients: ${this.recentPatients.length ? 'success' : 'failed'}`);
  }

}